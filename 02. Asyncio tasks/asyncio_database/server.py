import asyncio
from dictionary import *


async def handle_echo(reader, writer):
    com = Commands()
    data = await reader.readline()
    while data:
        message = data.decode("utf8")
        request = message[:len(message)-1]
        response = f'{com.command_check(request)}\n'
        addr = writer.get_extra_info('peername')

        print(f"Получено {request!r} от {addr!r}")

        writer.write(response.encode("utf8"))
        await writer.drain()
        print(f"Отпралено клиенту {addr!r} : {response[:len(response)-1]!r}")

        data = await reader.readline()

    print("_____________________________________")
    writer.close()

async def main():
    server = await asyncio.start_server(
        handle_echo, '127.0.0.1', 8889)

    addrs = ', '.join(str(sock.getsockname()) for sock in server.sockets)
    print(f'_____________________________________\nServing on {addrs}')

    async with server:
        await server.serve_forever()


asyncio.run(main())
