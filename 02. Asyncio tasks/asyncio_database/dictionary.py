from validators import Validator

shared_dict = {}
val = Validator()


class Commands:

    def __init__(self):
        self.shared_dict = shared_dict
        self.val = val

    def set_val(self, head, body):
        try:
            data = {head: body}
            self.shared_dict.update(data)
            print('Содержимое базы данных:')
            for key, value in self.shared_dict.items():
                print(f'{key}: {value}')
            self.respons_from_set_val = 'Данные успешно записаны'
        except:
            self.respons_from_set_val = 'Что-то пошло не так, проверьте введенные значения'


    def get_val(self, head):
        data = self.val.result(head)
        # print(self.shared_dict.get(data[0]), self.shared_dict)
        self.respons_from_get_val = self.shared_dict.get(data[0])

    def command_check(self, message):
        mes = message.split(' ')
        if mes[0] == 'set_val' and len(mes) == 3:
            self.type_data(mes[1], mes[2])
            response = self.respons_from_set_val
        elif mes[0] == 'get_val' and len(mes) == 2:
            self.get_val(mes[1])
            response = self.respons_from_get_val
        else:
            response = "Ошибка при вводе команды или её содержимого. " \
                       "Команда set_value состоит из трех значений без пробелов (команда, ключ, значение). " \
                       "Например: set_val lol kek. " \
                       "Команда get_val состоит из двух значений без пробелов (команда, ключ) Например: get_val lol."
        return response

    def type_data(self, head, body):
        data = self.val.result(head, body)
        self.set_val(data[0], data[1])
