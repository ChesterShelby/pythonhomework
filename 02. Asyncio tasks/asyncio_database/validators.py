class Validator:

    def number_head(self):
        try:
            if isinstance(int(self.head), int):
                return True
        except:
            return False

    def number_body(self):
        try:
            if isinstance(int(self.body), int):
                return True
        except:
            return False

    def result(self, head=None, body=None):
        self.head = head
        self.body = body
        if self.number_body():
            self.body = int(self.body)
        if self.number_head():
            self.head = int(self.head)
        return [self.head, self.body]

