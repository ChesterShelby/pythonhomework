import asyncio


async def get_command(message):
    reader, writer = await asyncio.open_connection(
        '127.0.0.1', 8889)

    print(f'Отправлено на сервер: {message[:len(message)-1]}')
    writer.write(message.encode("utf8"))

    data = await reader.readline()
    response = data.decode("utf8")
    print(f'Ответ сервера: {response[:len(response)-1]}')
    # print("_____________________________________")
    writer.close()


async def main():
    print("_____________________________________\n"
          "Команда set_value состоит из трех значений без пробелов (команда, ключ, значение)\n"
          "Например: set_val lol kek\n"
          "Команда get_val состоит из двух значений без пробелов (команда, ключ)\n"
          "Например: get_val lol")
    while True:
        print("_____________________________________")
        message = input() + '\n'
        await asyncio.gather(*[get_command(message)])


if __name__ == '__main__':
    asyncio.run(main())


