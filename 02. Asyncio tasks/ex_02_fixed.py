import asyncio
from urllib.parse import urlparse


async def print_http_response(url: str, port: int):
    url = urlparse(url)
    reader, writer = await asyncio.open_connection(url.hostname, port, ssl=True)
    query = (
        f"HEAD {url.path or '/'} HTTP/1.0\r\n"
        f"Host: {url.hostname}\r\n"
        f"\r\n"
    )

    writer.write(query.encode('utf-8'))
    while True:
        line = await reader.readline()
        if not line:
            break

        line = line.decode('utf-8').rstrip()
        if line:
            print(f'{url.hostname} |  {line}', end='\n')
    writer.close()


async def main():
    resources_lst = [
        ('https://www.python.org/', 443),
        ('https://www.vk.com/', 443)
    ]
    for url, port in resources_lst:
        await print_http_response(url, port)


if __name__ == '__main__':
    asyncio.run(main())
